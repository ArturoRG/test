<?php
/**
 * Carga del modelo Menus...
 */
Load::models('menus');

class MenusController extends ApplicationController {

    /**
     * Obtiene una lista para paginar los menus
     */
    public function index($page=1) 
    {
        $menu = new Menus();
        $this->listMenus = $menu->getMenus($page);
    }
    
    /**
     * Crea un Registro
     */
    public function create ()
    {
        /**
         * Se verifica si el usuario envio el form (submit) y si ademas 
         * dentro del array POST existe uno llamado "menus"
         * el cual aplica la autocarga de objeto para guardar los 
         * datos enviado por POST utilizando autocarga de objeto
         */
        if($this->has_post('menus')){
            /**
             * se le pasa al modelo por constructor los datos del form y ActiveRecord recoge esos datos
             * y los asocia al campo correspondiente siempre y cuando se utilice la convención
             * model.campo
             */
            $menu = new Menus($this->post('menus'));
            //En caso que falle la operación de guardar
            if(!$menu->save()){
                Flash::error('Falló Operación');
                //se hacen persistente los datos en el formulario
                $this->menus = $this->post('menus');
                /**
                 * NOTA: para que la autocarga aplique de forma correcta, es necesario que llame a la variable de instancia
                 * igual como esta el model de la vista, en este caso el model es "menus" y quedaria $this->menus
                 */
            }else{
                Flash::success('Operación exitosa');
            }
        }
    }

    /**
     * Edita un Registro
     */
    public function edit($id = null)
    {
    	$menu = new Menus();
        if($id != null){
    	    //Aplicando la autocarga de objeto, para comenzar la edición
            $this->menus = $menu->find((int)$id);
    	}
        //se verifica si se ha enviado el formulario (submit)
        if($this->has_post('menus')){
            
            if(!$menu->update($this->post('menus'))){
                Flash::error('Falló Operación');
                //se hacen persistente los datos en el formulario
                $this->menus = $this->post('menus');
            } else {
                Router::route_to('action: index');
            }
        }
    }

    /**
     * Eliminar un menu
     * 
     * @param int $id
     */
    public function del($id = null)
    {
        $menu = new Menus();
        if ($id) {
            if (!$menu->delete((int)$id)) {
                Flash::error('Falló Operación');
            }
        }
        //enrutando al index para listar los menus
        Router::route_to('action: index');
    }
}